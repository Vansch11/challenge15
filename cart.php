<?php 
class Cart {
    private $cartItems;

    public function __construct() {
        $this->cartItems = [];
    }

    public function addToCart($item) {
        $this->cartItems[] = $item;
    }

    public function printReceipt() {
        if (empty($this->cartItems)) {
            echo "Your cart is empty.\n";
            return;
        }

        $totalPrice = 0;

        foreach ($this->cartItems as $item) {
            $amount = $item['amount'];
            $product = $item['item'];
            $price = $product->getPrice();

            $itemName = $product->name;
            $unit = $product->sellingByKg ? 'kgs' : 'gunny sacks';
            $subtotal = $price * $amount;
            $totalPrice += $subtotal;

            echo "$itemName | $amount $unit | total= $subtotal denars\n";
        }

        echo "Final price amount: $totalPrice denars\n";
    }
}

?>
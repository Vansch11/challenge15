<?php 
require_once 'Product.php';
require_once 'MarketStall.php';
require_once 'Cart.php';


$orange = new Product('Orange', 35, true);
$potato = new Product('Potato', 240, false);
$nuts = new Product('Nuts', 850, true);
$kiwi = new Product('Kiwi', 670, false);
$pepper = new Product('Pepper', 330, true);
$raspberry = new Product('Raspberry', 555, false);


$marketStall1 = new MarketStall([$orange, $nuts, $pepper]);
$marketStall2 = new MarketStall([$potato, $kiwi, $raspberry]);


$cart = new Cart();


$cart->addToCart($marketStall1->getItem('orange', 3));
$cart->addToCart($marketStall1->getItem('pepper', 1));
$cart->addToCart($marketStall2->getItem('kiwi', 2));


$cart->printReceipt();
  

?>


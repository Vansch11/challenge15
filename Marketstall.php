<?php 
class MarketStall {
    public $products;

    public function __construct($products) {
        $this->products = $products;
    }

    public function addProductToMarket($product) {
        $this->products[] = $product;
    }

    public function getItem($itemName, $amount) {
        foreach ($this->products as $product) {
            if ($product->name === $itemName) {
                return ['amount' => $amount, 'item' => $product];
            }
        }
        return false;
    }
}
?>